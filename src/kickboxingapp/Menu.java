/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kickboxingapp;

import com.google.gson.JsonObject;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.awt.Toolkit;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import com.mongodb.gridfs.*;
import com.mongodb.util.JSON;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;
import javax.swing.table.DefaultTableModel;
import com.placeholder.PlaceHolder;
import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.Icon;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Gtec Software
 */
public class Menu extends javax.swing.JFrame {

    /**
     * Creates new form Menu
     */
    private MongoClient mongoClient;
    DB db;
    DefaultTableModel modelo;
    DefaultTableModel modelo2;
    DefaultTableModel modelo3;
    DefaultTableModel modelo4;

    public Menu() {
        initComponents();
        mongoClient = new MongoClient();
        db = mongoClient.getDB("KickBoxing");
        java.util.Date fecha = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        txtPrecioCF.setText("15");
        txtPrecioCN.setText("25");
        txtFechaIngreso.setText(dateFormat.format(fecha));

        CrearModelo();
        CrearModelo2();
        CrearModeloPago();
        CrearModelo2Pago();
        verAlumnos(1);
        verAlumnos();
        verAlumnosPago();
        verAlumnos2Pago();
    }

    private void CrearModeloPago() {
        try {
            modelo3 = (new DefaultTableModel(
                    null, new String[]{
                        "NUMERO DE ALUMNO",
                        "NOMBRE", "DEUDA", "LIMITE DEUDA"}) {
                Class[] types = new Class[]{
                    java.lang.Integer.class,
                    java.lang.String.class,
                    java.lang.Integer.class,
                    java.lang.Integer.class
                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, false
                };

                @Override
                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                @Override
                public boolean isCellEditable(int rowIndex, int colIndex) {
                    return canEdit[colIndex];
                }
            });
            jtbPagoClasesVer.setModel(modelo3);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString() + "error2");
        }
    }

    private void CrearModelo2Pago() {
        try {
            modelo4 = (new DefaultTableModel(
                    null, new String[]{
                        "NUMERO DE ALUMNO",
                        "NOMBRE", "PAGO", "FECHA", "HORA"}) {
                Class[] types = new Class[]{
                    java.lang.Integer.class,
                    java.lang.String.class,
                    java.lang.Integer.class,
                    java.lang.String.class,
                    java.lang.String.class
                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, false
                };

                @Override
                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                @Override
                public boolean isCellEditable(int rowIndex, int colIndex) {
                    return canEdit[colIndex];
                }
            });
            jtbPagoClasesHoy.setModel(modelo4);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString() + "error2");
        }
    }

    private void verAlumnosPago() {
        modelo3.setRowCount(0);
        try {

            DBCollection coll = db.getCollection("Personas");
            DBCursor curs = coll.find();
            while (curs.hasNext()) {
                DBObject o = curs.next();
                if (Integer.parseInt(o.get("deuda").toString()) > 0) {
                    Integer idA = (Integer) o.get("id");
                    String nombre = (String) o.get("nombre");
                    Integer fecha = (Integer) o.get("deuda");
                    Integer hora = (Integer) o.get("limite_deuda");
                    modelo3.addRow(new Object[]{idA, nombre, fecha, hora});
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void verAlumnosPagoById(int id) {
        modelo3.setRowCount(0);
        try {

            DBCollection coll = db.getCollection("Personas");
            BasicDBObject query = new BasicDBObject("id", id);
            DBCursor curs = coll.find(query);
            while (curs.hasNext()) {
                DBObject o = curs.next();
                if (Integer.parseInt(o.get("deuda").toString()) > 0) {
                    Integer idA = (Integer) o.get("id");
                    String nombre = (String) o.get("nombre");
                    Integer fecha = (Integer) o.get("deuda");
                    Integer hora = (Integer) o.get("limite_deuda");
                    modelo3.addRow(new Object[]{idA, nombre, fecha, hora});
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void verAlumnos2Pago() {
        modelo4.setRowCount(0);
        try {
            java.util.Date fechas = new Date();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            DBCollection coll = db.getCollection("PagoClases");

            DBCursor curs = coll.find();
            while (curs.hasNext()) {
                DBObject o = curs.next();
                if (o.get("fecha").equals(dateFormat.format(fechas))) {
                    Integer idA = (Integer) o.get("id");
                    String nombre = (String) o.get("nombre");
                    Integer pago = (Integer) o.get("pago");
                    //String fecha = dateFormat.format(fechas);
                    //String hora = hourFormat.format(fechas);
                    String fecha = (String) o.get("fecha");
                    String hora = (String) o.get("hora");
                    modelo4.addRow(new Object[]{idA, nombre, pago, fecha, hora});
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

//------------------------------------
    private void verAlumnos() {
        modelo2.setRowCount(0);
        System.out.println("exito1");
        try {
            DBCollection coll = db.getCollection("Personas");
            DBCursor curs = coll.find();
            while (curs.hasNext()) {
                DBObject o = curs.next();
                Integer idA = (Integer) o.get("id");
                String nombre = (String) o.get("nombre");
                modelo2.addRow(new Object[]{idA, nombre});
            }
        } catch (Exception e) {
            System.err.println("Error: " + e);
        }
    }
    String imagen = "";

    private void CrearModelo2() {
        try {
            modelo2 = (new DefaultTableModel(
                    null, new String[]{
                        "Numero de alumno",
                        "nombre"}) {
                Class[] types = new Class[]{
                    java.lang.Integer.class,
                    java.lang.String.class
                };
                boolean[] canEdit = new boolean[]{
                    false, false
                };

                @Override
                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                @Override
                public boolean isCellEditable(int rowIndex, int colIndex) {
                    return canEdit[colIndex];
                }
            });
            jTable2.setModel(modelo2);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString() + "error2");
        }
    }

    private void CrearModelo() {
        try {
            modelo = (new DefaultTableModel(
                    null, new String[]{
                        "NUMERO DE ALUMNO",
                        "NOMBRE", "FECHA ASISTENCIA", "HORA ASISTENCIA", "ES FIN DE SEMANA"}) {
                Class[] types = new Class[]{
                    java.lang.Integer.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class
                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, false, false
                };

                @Override
                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                @Override
                public boolean isCellEditable(int rowIndex, int colIndex) {
                    return canEdit[colIndex];
                }
            });
            jTable1.setModel(modelo);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString() + "error2");
        }
    }

    private void verAsistenciaPorDia() {
        modelo.setRowCount(0);
        try {
            java.util.Date fechas = new Date();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            DBCollection coll = db.getCollection("Asistencia");
            DBCursor curs = coll.find();
            String fechaABuscar = txtBuscarXDia.getText();
            boolean validacion = true;
            String expre = "\\d{1,2}/\\d{1,2}/\\d{4}";
            if (Pattern.matches(expre, txtBuscarXDia.getText()) && fechaABuscar != null && fechaABuscar.length() > 0) {
                validacion = true;
            } else {
                validacion = false;
                JOptionPane.showMessageDialog(null, "Formato de fecha incorrecto\n" + "Ejemplo 30/10/2019\nNo puede dejar vacio el campo");
            }
            if (validacion == true) {
                boolean vacio = true;
                while (curs.hasNext()) {
                    DBObject o = curs.next();
                    if (o.get("fecha_Asis").equals(fechaABuscar)) {
                        vacio = false;
                        Integer idA = (Integer) o.get("id");
                        String nombre = (String) o.get("nombre");
                        String fecha = (String) o.get("fecha_Asis");
                        String hora = (String) o.get("hora");
                        String find = (String) o.get("finSemana");
                        modelo.addRow(new Object[]{idA, nombre, fecha, hora, find});
                    }
                }
                if (vacio) {
                    JOptionPane.showMessageDialog(null, "No existen registro de clases en el dia: " + fechaABuscar);
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }

    public byte[] loadImage64(String url) throws Exception {
        File file = new File(url.toString());
        if (file.exists()) {
            int lenght = (int) file.length();
            BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
            byte[] bytes = new byte[lenght];
            reader.read(bytes, 0, lenght);
            reader.close();
            return bytes;
        } else {
//            log.info("Recurso no encontrado");
            return null;
        }
    }

    private void verAlumnos(int bandera) {
        modelo.setRowCount(0);
        try {
            java.util.Date fechas = new Date();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            DBCollection coll = db.getCollection("Asistencia");
            DBCursor curs = coll.find();
            if (bandera == 1) {
                while (curs.hasNext()) {
                    DBObject o = curs.next();
                    if (o.get("fecha_Asis").equals(dateFormat.format(fechas))) {
                        Integer idA = (Integer) o.get("id");
                        String nombre = (String) o.get("nombre");
                        String fecha = (String) o.get("fecha_Asis");
                        String hora = (String) o.get("hora");
                        String find = (String) o.get("finSemana");
                        modelo.addRow(new Object[]{idA, nombre, fecha, hora, find});
                    }
                }
            } else {
                System.out.println("entro a ver 2");
                while (curs.hasNext()) {
                    DBObject o = curs.next();
                    Integer idA = (Integer) o.get("id");
                    String nombre = (String) o.get("nombre");
                    String fecha = (String) o.get("fecha_Asis");
                    String hora = (String) o.get("hora");
                    String find = (String) o.get("finSemana");
                    modelo.addRow(new Object[]{idA, nombre, fecha, hora, find});

                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void verAlumnosById(int id) {
        try {
            modelo.setRowCount(0);
            DBCollection coll = db.getCollection("Asistencia");
            BasicDBObject query = new BasicDBObject("id", id);
            DBCursor curs = coll.find(query);
            boolean band = true;
            while (curs.hasNext()) {
                band = false;
                DBObject o = curs.next();
                Integer idA = (Integer) o.get("id");
                String nombre = (String) o.get("nombre");
                String fecha = (String) o.get("fecha_Asis");
                String hora = (String) o.get("hora");
                String find = (String) o.get("finSemana");
                modelo.addRow(new Object[]{idA, nombre, fecha, hora, find});
            }
            if (band == true) {
                JOptionPane.showMessageDialog(null, "El alumno no ha registrado ingreso");
                verAlumnos(1);
            }
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Error: " + nfe.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        btnVerById = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        txtIdAlumno = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        txtBuscarXDia = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        txtReistraClase = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtIdAlumnoInfo = new javax.swing.JTextField();
        btnVerById1 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        txtNombreI = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtTelAlumnoI = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtTelUrgI = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtNacI = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtDeudaI = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtObsI = new javax.swing.JTextArea();
        jLabel21 = new javax.swing.JLabel();
        txtIngresoI = new javax.swing.JTextField();
        txtImagenObt = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jtbPagoClasesVer = new javax.swing.JTable();
        txtBuscarDeudor = new javax.swing.JTextField();
        btnBuscarDeudor = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        txtPago = new javax.swing.JTextField();
        jButton8 = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jtbPagoClasesHoy = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtTelefonoAlumno = new javax.swing.JTextField();
        txtTelEmergencia = new javax.swing.JTextField();
        txtFechaNac = new javax.swing.JTextField();
        txtFechaIngreso = new javax.swing.JTextField();
        txtPrecioCN = new javax.swing.JTextField();
        txtPrecioCF = new javax.swing.JTextField();
        txtLimiteDeuda = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtObservaciones = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        labelImagen = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        pruebastxtx = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel11.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel11.setText("NUMERO DE ALUMNO");

        btnVerById.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        btnVerById.setText("BUSCAR POR ALUMNO");
        btnVerById.setActionCommand("");
        btnVerById.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerByIdActionPerformed(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jButton4.setText("REGISTRAR CLASE");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        txtIdAlumno.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        txtIdAlumno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdAlumnoActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jButton3.setText("VER ASISTENCIAS DEL DIA");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        txtBuscarXDia.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        txtBuscarXDia.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtBuscarXDiaFocusGained(evt);
            }
        });

        jButton5.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jButton5.setText("BUSCAR POR DIA");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        txtReistraClase.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        txtReistraClase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReistraClaseActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(jTable1);

        jLabel12.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel12.setText("FECHA:");

        jLabel13.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel13.setText("NUMERO DE ALUMNO");

        jButton6.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jButton6.setText("VER TODAS ASISTENCIAS");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jButton3)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel11)
                            .addComponent(btnVerById)
                            .addComponent(txtIdAlumno, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                            .addComponent(jButton4)
                            .addComponent(txtReistraClase)
                            .addComponent(jButton5)
                            .addComponent(txtBuscarXDia)))
                    .addComponent(jButton6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 839, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 610, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIdAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnVerById)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtReistraClase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarXDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5)
                        .addGap(51, 51, 51)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton6)))
                .addContainerGap(61, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("REGISTRO DE CLASE", jPanel2);

        jLabel14.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel14.setText("NUMERO DE ALUMNO");

        txtIdAlumnoInfo.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        txtIdAlumnoInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdAlumnoInfoActionPerformed(evt);
            }
        });

        btnVerById1.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        btnVerById1.setText("VER DATOS DE ALUMNO");
        btnVerById1.setActionCommand("");
        btnVerById1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerById1ActionPerformed(evt);
            }
        });

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(jTable2);

        jPanel5.setBackground(new java.awt.Color(204, 255, 255));

        jLabel15.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel15.setText("NOMBRE");

        txtNombreI.setEditable(false);
        txtNombreI.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        jLabel16.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel16.setText("TELEFONO ALUMNO");

        txtTelAlumnoI.setEditable(false);
        txtTelAlumnoI.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        jLabel17.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel17.setText("TELEFONO URGENCIAS");

        txtTelUrgI.setEditable(false);
        txtTelUrgI.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        jLabel18.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel18.setText("FECHA DE NACIMIENTO");

        txtNacI.setEditable(false);
        txtNacI.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        jLabel19.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel19.setText("FECHA DE INGRESO");

        txtDeudaI.setEditable(false);
        txtDeudaI.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        jLabel20.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel20.setText("OBSERVACIONES");

        txtObsI.setEditable(false);
        txtObsI.setColumns(20);
        txtObsI.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        txtObsI.setRows(5);
        jScrollPane4.setViewportView(txtObsI);

        jLabel21.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel21.setText("LIMITE DE CREDITO: ");

        txtIngresoI.setEditable(false);
        txtIngresoI.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel18)
                            .addComponent(jLabel17)
                            .addComponent(jLabel19)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                            .addComponent(txtIngresoI, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNacI, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTelUrgI)
                            .addComponent(txtDeudaI)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16))
                        .addGap(56, 56, 56)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNombreI, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                            .addComponent(txtTelAlumnoI))))
                .addGap(42, 42, 42)
                .addComponent(txtImagenObt, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(txtNombreI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTelAlumnoI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTelUrgI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNacI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(txtIngresoI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(txtImagenObt, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtDeudaI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(149, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnVerById1)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 439, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtIdAlumnoInfo, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIdAlumnoInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnVerById1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap(146, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("INFORMACION DE ALUMNO", jPanel3);

        jLabel22.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel22.setText("NUMERO DE ALUMNO");

        jtbPagoClasesVer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane5.setViewportView(jtbPagoClasesVer);

        txtBuscarDeudor.setFont(new java.awt.Font("Comic Sans MS", 0, 24)); // NOI18N

        btnBuscarDeudor.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        btnBuscarDeudor.setText("BUSCAR DEUDOR");
        btnBuscarDeudor.setActionCommand("");
        btnBuscarDeudor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarDeudorActionPerformed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel23.setText("NUMERO DE ALUMNO");

        txtPago.setFont(new java.awt.Font("Comic Sans MS", 0, 24)); // NOI18N

        jButton8.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jButton8.setText("PAGAR");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jtbPagoClasesHoy.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane6.setViewportView(jtbPagoClasesHoy);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel22)
                                .addComponent(btnBuscarDeudor)
                                .addComponent(txtBuscarDeudor)
                                .addComponent(jButton8)
                                .addComponent(txtPago))
                            .addComponent(jLabel23))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane5))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1358, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel22)
                        .addGap(16, 16, 16)
                        .addComponent(txtBuscarDeudor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscarDeudor)
                        .addGap(11, 11, 11)
                        .addComponent(jLabel23)
                        .addGap(5, 5, 5)
                        .addComponent(txtPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jButton8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(69, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("PAGO DE CLASES", jPanel4);

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel1.setText("TELEFONO DEL ALUMNO:");

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel2.setText("IMAGEN:");

        txtNombre.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel3.setText("TELEFONO DE EMERGENCIA:");

        jLabel4.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel4.setText("FECHA DE NACIMIENTO:");

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel5.setText("FECHA DE INGRESO");

        jLabel6.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel6.setText("PRECIO DE CLASE NORMAL:");

        jLabel7.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel7.setText("PRECIO DE CLASE FIN DE SEMANA:");

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel8.setText("LIMITE DE DEUDA:");

        jLabel9.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel9.setText("OBSERVACIONES:");

        txtTelefonoAlumno.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        txtTelEmergencia.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        txtFechaNac.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        txtFechaIngreso.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        txtPrecioCN.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        txtPrecioCF.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        txtLimiteDeuda.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        txtObservaciones.setColumns(20);
        txtObservaciones.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        txtObservaciones.setRows(5);
        jScrollPane1.setViewportView(txtObservaciones);

        jLabel10.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel10.setText("NOMBRE:");

        jButton1.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jButton1.setText("CARGAR IMAGEN");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        labelImagen.setBackground(new java.awt.Color(0, 0, 0));
        labelImagen.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jButton2.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jButton2.setText("REGISTRAR ALUMNO");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        pruebastxtx.setBackground(new java.awt.Color(0, 0, 0));
        pruebastxtx.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel9)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jScrollPane1))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel8)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtLimiteDeuda, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtTelEmergencia, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel7)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtPrecioCF, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel5)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtFechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtTelefonoAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel6)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtPrecioCN, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jButton2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 115, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(labelImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(pruebastxtx, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelefonoAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelEmergencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPrecioCN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPrecioCF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLimiteDeuda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jButton2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(labelImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pruebastxtx, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(87, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("REGISTRO DE ALUMNO", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static byte[] LoadImage(String filePath) throws Exception {
        File file = new File(filePath);
        int size = (int) file.length();
        byte[] buffer = new byte[size];
        FileInputStream in = new FileInputStream(file);
        in.read(buffer);
        in.close();
        System.out.println(buffer);
        return buffer;
    }

    private Image getImage(byte[] bytes, boolean isThumbnail) throws IOException {
        String[] parts = imagen.split(Pattern.quote("."));
        System.out.println("nombre: " + imagen);
        System.out.println("split: " + parts[1]);
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName(parts[1]);
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        if (isThumbnail) {
            param.setSourceSubsampling(4, 4, 0, 0);
        }
        return reader.read(0, param);

    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(".jpg & .png", "jpg", "png");
            fileChooser.setFileFilter(filtro);
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File name = fileChooser.getSelectedFile();
                System.out.println(name);
                ImageIcon image = new ImageIcon(fileChooser.getSelectedFile().getPath());
                if (image.getIconHeight() > 342 || image.getIconWidth() > 230) {
                    ImageIcon imagenScalada = new ImageIcon(image.getImage().getScaledInstance(190, 200, 200));
                    imagen = fileChooser.getSelectedFile().getPath();
                    labelImagen.setIcon(imagenScalada);
                    //imagen=fileChooser.getI;
                } else {
                    imagen = fileChooser.getSelectedFile().getPath();
                    //JOptionPane.showMessageDialog(null, fis);
                    labelImagen.setIcon(image);
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        try {
            byte[] imagenByte = null;
            //GridFSInputFile in = null;
            //System.out.println(imagen);
            if (imagen != null && imagen != "") {
                imagenByte = LoadImage(imagen);
                //GridFS fs = new GridFS(db);
                //in = fs.createFile(imagenByte);
                //in.save();
                //Image imagen2 = getImage(imagenByte, false);
                //Icon icon = new ImageIcon(imagen2);                
            }
            DBCollection coll = db.getCollection("Personas");
            BasicDBObject doc = new BasicDBObject();
            DBCursor curs = coll.find().sort(new BasicDBObject("id", -1));
            if (curs.hasNext()) {
                DBObject o = curs.next();
                doc.put("id", (Integer.parseInt(o.get("id").toString())) + 1);
            } else {
                doc.put("id", 1);
            }
            if (imagenByte != null) {
                doc.put("imagen", imagenByte);
            }
            boolean validacion = true;
            //JOptionPane.showMessageDialog(null, Integer.parseInt(txtTelEmergencia.getText()));

            String txtTE = txtTelEmergencia.getText().toString();
            if (txtTE.length() > 9) {
                System.out.println("valido");
            } else {
                validacion = false;
                JOptionPane.showMessageDialog(null, "El numero de telefono para emergencia no debe tener letras ni caracteres especiales\n"
                        + "Ingrese un numero de telefono valido\n" + "Ejemplo: 1234567890");
            }
            String txtTA = txtTelefonoAlumno.getText().toString();
            if (txtTA.length() > 9) {
                System.out.println("valido");
            } else {
                validacion = false;
                JOptionPane.showMessageDialog(null, "El numero de telefono del alumno no debe tener letras ni caracteres especiales\n"
                        + "Ingrese un numero de telefono valido\n" + "Ejemplo: 1234567890");
            }
            String expre = "\\d{1,2}/\\d{1,2}/\\d{4}";
            if (Pattern.matches(expre, txtFechaNac.getText())) {

            } else {
                validacion = false;
                JOptionPane.showMessageDialog(null, "Formato de fecha incorrecto\n" + "Ejemplo 30/10/2019");
            }
            try {
                Integer.parseInt(txtPrecioCN.getText());
                if (Integer.parseInt(txtPrecioCN.getText()) < 0) {
                    validacion = false;
                    JOptionPane.showMessageDialog(null, "El precio de las clases normales debe ser mayor a 0\n No acepta letras");
                }
            } catch (NumberFormatException ex) {
                validacion = false;
                JOptionPane.showMessageDialog(null, "El precio de las clases normales debe ser mayor a 0\n No acepta letras");
            }
            try {
                Integer.parseInt(txtPrecioCF.getText());
                if (Integer.parseInt(txtPrecioCF.getText()) < 0) {
                    validacion = false;
                    JOptionPane.showMessageDialog(null, "El precio de las clases en fines de semana debe ser mayor a 0\n No acepta letras");
                }
            } catch (NumberFormatException ex) {
                validacion = false;
                JOptionPane.showMessageDialog(null, "El precio de las clases en fines de semana debe ser mayor a 0\n No acepta letras");
            }
            try {
                Integer.parseInt(txtLimiteDeuda.getText());
                if (Integer.parseInt(txtLimiteDeuda.getText()) < 0) {
                    validacion = false;
                    JOptionPane.showMessageDialog(null, "El limite de la deuda debe ser mayor a 0\n No acepta letras");
                }
            } catch (NumberFormatException ex) {
                validacion = false;
                JOptionPane.showMessageDialog(null, "El limite de la deuda debe ser mayor a 0\n No acepta letras");
            }
            if (validacion == true) {
                try {
                    doc.put("nombre", txtNombre.getText());
                    doc.put("numero_alumno", txtTelefonoAlumno.getText());
                    doc.put("numero_emergencia", txtTelEmergencia.getText());
                    doc.put("fecha_nacimiento", txtFechaNac.getText());
                    doc.put("fecha_ingreso", txtFechaIngreso.getText());
                    doc.put("observaciones", txtObservaciones.getText());
                    doc.put("pago_clasesN", Integer.parseInt(txtPrecioCN.getText()));
                    doc.put("pago_clasesF", Integer.parseInt(txtPrecioCF.getText()));
                    doc.put("limite_deuda", (Integer.parseInt(txtLimiteDeuda.getText())));
                    doc.put("saldo", 0);
                    doc.put("deuda", 0);
                    coll.insert(doc);
                    JOptionPane.showMessageDialog(null, "Alumno Registrado Correctamente");
                    verAlumnos();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Error al guardar datos: " + e);
                }

            }
            //GridFSDBFile out = fs.findOne(new BasicDBObject("_id", in.getId()));
            //FileOutputStream outputImage = new FileOutputStream("C:/FabTodos/prueba.png");
            //out.writeTo(outputImage);
            //outputImage.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        verAlumnos(2);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void txtReistraClaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReistraClaseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReistraClaseActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        verAsistenciaPorDia();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void txtBuscarXDiaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtBuscarXDiaFocusGained
        // TODO add your handling code here:
        //if (txtBuscarXDia.getText().length() > 2) {
        //    txtBuscarXDia.setText(txtBuscarXDia.getText() + "/");
        //}
    }//GEN-LAST:event_txtBuscarXDiaFocusGained

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        verAlumnos(1);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txtIdAlumnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdAlumnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdAlumnoActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        try {
            if (txtReistraClase.getText().length() > 0) {
                System.out.println("Entro");
                java.util.Date fecha = new Date();
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                DateFormat hourFormat = new SimpleDateFormat("HH:mm");
                DBCollection coll = db.getCollection("Asistencia");
                DBCollection coll2 = db.getCollection("Personas");
                BasicDBObject doc = new BasicDBObject();
                BasicDBObject query = new BasicDBObject("id", Integer.parseInt(txtReistraClase.getText()));
                DBCursor curs = coll.find(query);//Asistencia
                DBCursor curs2 = coll2.find(query);//Personas
                boolean band = false;
                boolean bandP = false;
                if (curs2.hasNext()) {
                    System.out.println("Entro a if");
                    DBObject o = curs2.next();
                    int saldo = Integer.parseInt(o.get("saldo").toString());
                    int deuda = Integer.parseInt(o.get("deuda").toString());
                    int limite_deuda = Integer.parseInt(o.get("limite_deuda").toString());
                    int pago_clasesN = Integer.parseInt(o.get("pago_clasesN").toString());
                    int pago_clasesF = Integer.parseInt(o.get("pago_clasesF").toString());
                    //si y a existe un registro en la bd del usuario el mismo dia pide confirmar si desea volver a registrarse
                    //JOptionPane.showMessageDialog(null, "Dato obtenido de la bd: " + asist.get("fecha_Asis").toString() + " dia de hoy: " + dateFormat.format(fecha));
                    if (curs.hasNext()) {
                        System.out.println("Entro 1");
                        DBObject asist = curs.next();
                        //JOptionPane.showMessageDialog(null, asist.get("fecha_Asis").toString().equals(dateFormat.format(fecha)));
                        if (asist.get("fecha_Asis").toString().equals(dateFormat.format(fecha)) == true) {

                            int input = JOptionPane.showConfirmDialog(null, "Desea volver a registrar entrada de alumno");
                            if (input == 0) {
                                band = true;
                            } else {
                                band = false;
                            }
                        } else {
                            //JOptionPane.showMessageDialog(null, asist.get("fecha_Asis").toString().equals(dateFormat.format(fecha)));
                            band = true;
                        }
                    } else {
                        band = true;
                    }

                    String mnsj = "";
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(fecha);
                    int diaS = cal.get(Calendar.DAY_OF_WEEK);

                    //empieza area de fin de semana
                    if (diaS == 1 || diaS == 6 || diaS == 7) {
                        System.out.println("Entro 2: " + band);
                        if (band == true) {
                            System.out.println("Entro 3");
                            if (saldo > 0) {
                                System.out.println("Entro 4");
                                if ((saldo - pago_clasesF) < 0) {
                                    System.out.println("Entro 5");
                                    int aux = (saldo - pago_clasesF) * -1;
                                    if (limite_deuda <= aux) {
                                        System.out.println("Entro 6");
                                        //Se actualiza saldo a 0
                                        BasicDBObject newDocument = new BasicDBObject();
                                        newDocument.append("$set", new BasicDBObject().append("saldo", 0));
                                        BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry, newDocument);
                                        //se actualiza deuda agregando la deuda
                                        BasicDBObject newDocument2 = new BasicDBObject();
                                        newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + aux));
                                        BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry2, newDocument2);
                                        bandP = true;
                                    } else {
                                        System.out.println("Entro 7");
                                        mnsj = "A excedido su deuda no puede tomar la clase";
                                    }
                                } else {
                                    System.out.println("Entro 8");
                                    //Se resta solamente el pago a saldo
                                    BasicDBObject newDocument = new BasicDBObject();
                                    newDocument.append("$set", new BasicDBObject().append("saldo", saldo - pago_clasesF));
                                    BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                    coll2.update(qry, newDocument);
                                    bandP = true;
                                }

                            } else {
                                System.out.println("Entro 9");
                                //se genera deuda y se compara con limite de deuda
                                if ((deuda + pago_clasesF) > limite_deuda) {
                                    System.out.println("Entro 10");
                                    mnsj = "A excedido su deuda no puede tomar la clase";
                                } else {
                                    System.out.println("Entro 11");
                                    BasicDBObject newDocument2 = new BasicDBObject();
                                    newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + pago_clasesF));
                                    BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                    coll2.update(qry2, newDocument2);
                                    bandP = true;
                                }
                            }
                        }
                        //termina area de fin de semana
                    } else {
                        //area
                        if (diaS == 1 || diaS == 6 || diaS == 7) {
                            if (band == true) {
                                if (saldo > 0) {
                                    if ((saldo - pago_clasesF) < 0) {
                                        System.out.println("Entro 12");
                                        int aux = (saldo - pago_clasesF) * -1;
                                        if (limite_deuda <= aux) {
                                            System.out.println("Entro 13");
                                            //Se actualiza saldo a 0
                                            BasicDBObject newDocument = new BasicDBObject();
                                            newDocument.append("$set", new BasicDBObject().append("saldo", 0));
                                            BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                            coll2.update(qry, newDocument);
                                            //se actualiza deuda agregando la deuda
                                            BasicDBObject newDocument2 = new BasicDBObject();
                                            newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + aux));
                                            BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                            coll2.update(qry2, newDocument2);

                                            bandP = true;
                                        } else {
                                            System.out.println("Entro 14");
                                            mnsj = "A excedido su deuda no puede tomar la clase";
                                        }
                                    } else {
                                        System.out.println("Entro 15");
                                        //Se resta solamente el pago a saldo
                                        BasicDBObject newDocument = new BasicDBObject();
                                        newDocument.append("$set", new BasicDBObject().append("saldo", saldo - pago_clasesF));
                                        BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry, newDocument);
                                        bandP = true;
                                    }
                                } else {
                                    //se genera deuda y se compara con limite de deuda
                                    if ((deuda + pago_clasesF) > limite_deuda) {
                                        System.out.println("Entro 16");
                                        mnsj = "A excedido su deuda no puede tomar la clase";
                                    } else {
                                        System.out.println("Entro 17");
                                        BasicDBObject newDocument2 = new BasicDBObject();
                                        newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + pago_clasesF));
                                        BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry2, newDocument2);
                                        bandP = true;
                                    }
                                }
                            }
                        } else {
                            if (band == true) {
                                if (saldo > 0) {
                                    if ((saldo - pago_clasesN) < 0) {
                                        int aux = (saldo - pago_clasesN) * -1;
                                        if (limite_deuda <= aux) {
                                            //Se actualiza saldo a 0
                                            System.out.println("Entro 18");
                                            BasicDBObject newDocument = new BasicDBObject();
                                            newDocument.append("$set", new BasicDBObject().append("saldo", 0));
                                            BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                            coll2.update(qry, newDocument);
                                            //se actualiza deuda agregando la deuda
                                            BasicDBObject newDocument2 = new BasicDBObject();
                                            newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + aux));
                                            BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                            coll2.update(qry2, newDocument2);

                                            bandP = true;
                                        } else {
                                            mnsj = "A excedido su deuda no puede tomar la clase";
                                        }
                                    } else {
                                        System.out.println("Entro 19");
                                        //Se resta solamente el pago a saldo
                                        BasicDBObject newDocument = new BasicDBObject();
                                        newDocument.append("$set", new BasicDBObject().append("saldo", saldo - pago_clasesN));
                                        BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry, newDocument);
                                        bandP = true;
                                    }
                                } else {
                                    //se genera deuda y se compara con limite de deuda
                                    if ((deuda + pago_clasesN) > limite_deuda) {
                                        System.out.println("Entro 20");
                                        mnsj = "A excedido su deuda no puede tomar la clase";
                                    } else {
                                        BasicDBObject newDocument2 = new BasicDBObject();
                                        newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + pago_clasesN));
                                        BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry2, newDocument2);
                                        bandP = true;
                                    }
                                }
                            }
                        }
                    }
                    if (band == true) {
                        if (bandP == true) {
                            System.out.println("Entro 21");
                            doc.put("id", Integer.parseInt(txtReistraClase.getText()));
                            doc.put("nombre", o.get("nombre"));
                            doc.put("fecha_Asis", dateFormat.format(fecha));
                            doc.put("hora", hourFormat.format(fecha));
                            if (diaS == 1 || diaS == 6 && diaS == 7) {
                                doc.put("finSemana", "si");
                            } else {
                                doc.put("finSemana", "no");
                            }
                            coll.insert(doc);
                            verAlumnos(1);
                            JOptionPane.showMessageDialog(null, "Registro de asistencia correcto");
                        } else {
                            JOptionPane.showMessageDialog(null, mnsj);
                            verAlumnos(1);
                        }
                    } else {
                        verAlumnos(1);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "El numero de alumno no existe en el registro");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error: Ingrese un numero de alumno", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error: No debe ingresar letras", "Alerta", JOptionPane.WARNING_MESSAGE);
        } catch (Error e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btnVerByIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerByIdActionPerformed
        try {
            if (txtIdAlumno.getText().length() > 0) {
                int id = Integer.parseInt(txtIdAlumno.getText());
                verAlumnosById(id);
            } else {
                JOptionPane.showMessageDialog(null, "Error: No ingreso numero de alumno", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error: No debe ingresar letras", "Alerta", JOptionPane.WARNING_MESSAGE);
        }

        //JOptionPane.showMessageDialog(null, txtIdAlumno.getText().length());
    }//GEN-LAST:event_btnVerByIdActionPerformed

    private void txtIdAlumnoInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdAlumnoInfoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdAlumnoInfoActionPerformed

    private void btnVerById1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerById1ActionPerformed
        try {
            //GridFS fs = new GridFS(db);
            int id = Integer.parseInt(txtIdAlumnoInfo.getText());
            DBCollection coll = db.getCollection("Personas");
            BasicDBObject query = new BasicDBObject("id", id);
            DBCursor curs = coll.find(query);
            //System.out.println(curs.hasNext());
            if (curs.hasNext()) {
                DBObject o = curs.next();

                o.containsField("s");
                System.out.println("obteniendo imagen: " + o.get("imagen"));

                System.out.println("curs: " + o);

                if (o.get("imagen") != null && o.get("imagen") != "") {
                    System.out.println(o.get("imagen"));
                    String img = o.get("imagen").toString();
                    byte[] b = img.getBytes();
                    Image imagen2 = getImage(b, false);
                    Icon icon = new ImageIcon(imagen2);
                    txtImagenObt.setIcon(icon);
                }
                txtNombreI.setText(o.get("nombre").toString());
                txtTelAlumnoI.setText(o.get("numero_alumno").toString());
                txtTelUrgI.setText(o.get("numero_emergencia").toString());
                txtNacI.setText(o.get("fecha_nacimiento").toString());
                txtIngresoI.setText(o.get("fecha_ingreso").toString());
                txtObsI.setText(o.get("observaciones").toString());
                txtDeudaI.setText(o.get("deuda").toString());
            } else {
                JOptionPane.showMessageDialog(null, "No existe el alumno");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnVerById1ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        java.util.Date fecha = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat hourFormat = new SimpleDateFormat("HH:mm");

        DBCollection coll = db.getCollection("PagoClases");
        DBCollection coll2 = db.getCollection("Personas");
        BasicDBObject doc = new BasicDBObject();

        BasicDBObject query = new BasicDBObject("id", Integer.parseInt(txtPago.getText()));
        DBCursor curs = coll2.find(query);

        if (curs.hasNext()) {
            DBObject o = curs.next();
            int pago = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la cantidad a pagar"));
            int deuda = Integer.parseInt(o.get("deuda").toString());
            int saldo = Integer.parseInt(o.get("saldo").toString());
            if (deuda > 0) {
                int saldoA = deuda - pago;
                if (saldoA < 0) {
                    System.out.println("entro 1");
                    int saldoT = saldoA * -1;
                    BasicDBObject newDocument = new BasicDBObject();
                    newDocument.append("$set", new BasicDBObject().append("saldo", saldoT).append("deuda", 0));
                    BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtPago.getText()));
                    coll2.update(qry, newDocument);
                } else {
                    System.out.println("entro 2: " + pago);
                    BasicDBObject newDocument = new BasicDBObject();
                    newDocument.append("$set", new BasicDBObject().append("deuda", saldoA));
                    BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtPago.getText()));
                    coll2.update(qry, newDocument);
                }
            } else {

                BasicDBObject newDocument = new BasicDBObject();
                newDocument.append("$set", new BasicDBObject().append("saldo", pago + saldo));
                BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtPago.getText()));
                coll2.update(qry, newDocument);
            }

            doc.put("id", Integer.parseInt(txtPago.getText()));
            doc.put("nombre", o.get("nombre"));
            doc.put("pago", pago);
            doc.put("fecha", dateFormat.format(fecha));
            doc.put("hora", hourFormat.format(fecha));
            coll.insert(doc);
            verAlumnosPago();
            verAlumnos2Pago();
        } else {
            JOptionPane.showMessageDialog(null, "No existe el numero de control");
        }
    }//GEN-LAST:event_jButton8ActionPerformed

    private void btnBuscarDeudorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarDeudorActionPerformed
        // TODO add your handling code here:
        try {
            if (txtBuscarDeudor.getText().length() > 0) {
                int id = Integer.parseInt(txtBuscarDeudor.getText());
                verAlumnosPagoById(id);
            } else {
                JOptionPane.showMessageDialog(null, "Error: No ingreso numero de alumno", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error: No debe ingresar letras", "Alerta", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btnBuscarDeudorActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Menu menu = new Menu();
                //menu.setSize(Toolkit.getDefaultToolkit().getScreenSize());
                menu.setExtendedState(Menu.MAXIMIZED_BOTH);
                menu.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarDeudor;
    private javax.swing.JButton btnVerById;
    private javax.swing.JButton btnVerById1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jtbPagoClasesHoy;
    private javax.swing.JTable jtbPagoClasesVer;
    private javax.swing.JLabel labelImagen;
    private javax.swing.JLabel pruebastxtx;
    private javax.swing.JTextField txtBuscarDeudor;
    private javax.swing.JTextField txtBuscarXDia;
    private javax.swing.JTextField txtDeudaI;
    private javax.swing.JTextField txtFechaIngreso;
    private javax.swing.JTextField txtFechaNac;
    private javax.swing.JTextField txtIdAlumno;
    private javax.swing.JTextField txtIdAlumnoInfo;
    private javax.swing.JLabel txtImagenObt;
    private javax.swing.JTextField txtIngresoI;
    private javax.swing.JTextField txtLimiteDeuda;
    private javax.swing.JTextField txtNacI;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNombreI;
    private javax.swing.JTextArea txtObsI;
    private javax.swing.JTextArea txtObservaciones;
    private javax.swing.JTextField txtPago;
    private javax.swing.JTextField txtPrecioCF;
    private javax.swing.JTextField txtPrecioCN;
    private javax.swing.JTextField txtReistraClase;
    private javax.swing.JTextField txtTelAlumnoI;
    private javax.swing.JTextField txtTelEmergencia;
    private javax.swing.JTextField txtTelUrgI;
    private javax.swing.JTextField txtTelefonoAlumno;
    // End of variables declaration//GEN-END:variables

}
