/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kickboxingapp;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gtec Software
 */
public class RegistroClases extends javax.swing.JFrame {

    private MongoClient mongoClient;
    DB db;

    /**
     * Creates new form InfoAlumno
     */
    DefaultTableModel modelo;

    private void CrearModelo() {
        try {
            modelo = (new DefaultTableModel(
                    null, new String[]{
                        "NUMERO DE ALUMNO",
                        "NOMBRE", "FECHA ASISTENCIA", "HORA ASISTENCIA","ES FIN DE SEMANA"}) {
                Class[] types = new Class[]{
                    java.lang.Integer.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class,
                    java.lang.String.class
                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, false, false
                };

                @Override
                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                @Override
                public boolean isCellEditable(int rowIndex, int colIndex) {
                    return canEdit[colIndex];
                }
            });
            jTable1.setModel(modelo);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString() + "error2");
        }
    }

    public RegistroClases() {
        initComponents();
        CrearModelo();
        mongoClient = new MongoClient();
        db = mongoClient.getDB("KickBoxing");
        verAlumnos();
    }

    private void verAlumnos() {
        modelo.setRowCount(0);
        try {
            java.util.Date fechas = new Date();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            DBCollection coll = db.getCollection("Asistencia");
            DBCursor curs = coll.find();
            while (curs.hasNext()) {
                DBObject o = curs.next();
                if (o.get("fecha_Asis").equals(dateFormat.format(fechas))) {
                    Integer idA = (Integer) o.get("id");
                    String nombre = (String) o.get("nombre");
                    String fecha = (String) o.get("fecha_Asis");
                    String hora = (String) o.get("hora");
                    String find = (String) o.get("finSemana");
                    modelo.addRow(new Object[]{idA, nombre, fecha, hora, find});
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void verAlumnosById(int id) {
        try {
            modelo.setRowCount(0);
            DBCollection coll = db.getCollection("Asistencia");
            BasicDBObject query = new BasicDBObject("id", id);
            DBCursor curs = coll.find(query);
            if (curs.hasNext()) {
                DBObject o = curs.next();
                Integer idA = (Integer) o.get("id");
                String nombre = (String) o.get("nombre");
                String fecha = (String) o.get("fecha_Asis");
                String hora = (String) o.get("hora");
                String find = (String) o.get("finSemana");
                modelo.addRow(new Object[]{idA, nombre, fecha, hora, find});
            } else {
                JOptionPane.showMessageDialog(null, "El alumno no ha registrado ingreso");
                verAlumnos();
            }
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Error: " + nfe.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnVerById = new javax.swing.JButton();
        txtIdAlumno = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        txtReistraClase = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        txtBuscarXDia = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("NUMERO DE ALUMNO");

        btnVerById.setText("BUSCAR");
        btnVerById.setActionCommand("");
        btnVerById.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerByIdActionPerformed(evt);
            }
        });

        txtIdAlumno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdAlumnoActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(jTable1);

        jButton1.setText("VER ASISTENCIAS DEL DIA");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("REGISTRAR CLASE");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        txtReistraClase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReistraClaseActionPerformed(evt);
            }
        });

        jButton3.setText("BUSCAR POR DIA");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1)
                    .addComponent(btnVerById)
                    .addComponent(txtIdAlumno)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(txtReistraClase)
                    .addComponent(jButton3)
                    .addComponent(txtBuscarXDia))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 615, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtIdAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnVerById)
                        .addGap(23, 23, 23)
                        .addComponent(jButton1)
                        .addGap(34, 34, 34)
                        .addComponent(txtReistraClase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtBuscarXDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addGap(55, 55, 55))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtIdAlumnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdAlumnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdAlumnoActionPerformed

    private void btnVerByIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerByIdActionPerformed
        try {
            if (txtIdAlumno.getText().length() > 0) {
                int id = Integer.parseInt(txtIdAlumno.getText());
                verAlumnosById(id);
            } else {
                JOptionPane.showMessageDialog(null, "Error: No ingreso numero de alumno", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error: No debe ingresar letras", "Alerta", JOptionPane.WARNING_MESSAGE);
        }

        //JOptionPane.showMessageDialog(null, txtIdAlumno.getText().length());

    }//GEN-LAST:event_btnVerByIdActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        verAlumnos();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtReistraClaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReistraClaseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReistraClaseActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            if (txtReistraClase.getText().length() > 0) {
                java.util.Date fecha = new Date();
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                DateFormat hourFormat = new SimpleDateFormat("HH:mm");
                DBCollection coll = db.getCollection("Asistencia");
                DBCollection coll2 = db.getCollection("Personas");
                BasicDBObject doc = new BasicDBObject();
                BasicDBObject query = new BasicDBObject("id", Integer.parseInt(txtReistraClase.getText()));
                DBCursor curs = coll.find(query);//Asistencia
                DBCursor curs2 = coll2.find(query);//Personas
                boolean band = false;
                boolean bandP = false;
                if (curs2.hasNext()) {

                    DBObject o = curs2.next();
                    int saldo = Integer.parseInt(o.get("saldo").toString());
                    int deuda = Integer.parseInt(o.get("deuda").toString());
                    int limite_deuda = Integer.parseInt(o.get("limite_deuda").toString());
                    int pago_clasesN = Integer.parseInt(o.get("pago_clasesN").toString());
                    int pago_clasesF = Integer.parseInt(o.get("pago_clasesF").toString());

                    //si y a existe un registro en la bd del usuario el mismo dia pide confirmar si desea volver a registrarse
                    //JOptionPane.showMessageDialog(null, "Dato obtenido de la bd: " + asist.get("fecha_Asis").toString() + " dia de hoy: " + dateFormat.format(fecha));
                    while (curs.hasNext()) {
                        DBObject asist = curs.next();
                        if (asist.get("fecha_Asis").toString().equals(dateFormat.format(fecha)) == true) {
                            //JOptionPane.showMessageDialog(null, asist.get("fecha_Asis").toString().equals(dateFormat.format(fecha)));
                            int input = JOptionPane.showConfirmDialog(null, "Desea volver a registrar entrada de alumno");
                            if (input == 0) {
                                band = true;
                                break;
                            } else {
                                band = false;
                                break;
                            }
                        } else {
                            //JOptionPane.showMessageDialog(null, asist.get("fecha_Asis").toString().equals(dateFormat.format(fecha)));
                            band = true;
                        }
                    }

                    String mnsj = "";
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(fecha);
                    int diaS = cal.get(Calendar.DAY_OF_WEEK);
                    JOptionPane.showMessageDialog(null, diaS);

                    //empieza area de fin de semana
                    if (diaS == 1 || diaS == 6 || diaS == 7) {
                        if (band == true) {
                            if (saldo > 0) {
                                if ((saldo - pago_clasesF) < 0) {
                                    int aux = (saldo - pago_clasesF) * -1;
                                    if (limite_deuda <= aux) {
                                        //Se actualiza saldo a 0
                                        BasicDBObject newDocument = new BasicDBObject();
                                        newDocument.append("$set", new BasicDBObject().append("saldo", 0));
                                        BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry, newDocument);
                                        //se actualiza deuda agregando la deuda
                                        BasicDBObject newDocument2 = new BasicDBObject();
                                        newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + aux));
                                        BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry2, newDocument2);

                                        bandP = true;
                                    } else {
                                        mnsj = "A excedido su deuda no puede tomar la clase";
                                    }
                                } else {
                                    //Se resta solamente el pago a saldo
                                    BasicDBObject newDocument = new BasicDBObject();
                                    newDocument.append("$set", new BasicDBObject().append("saldo", saldo - pago_clasesF));
                                    BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                    coll2.update(qry, newDocument);
                                    bandP = true;
                                }
                            } else {
                                //se genera deuda y se compara con limite de deuda
                                if ((deuda + pago_clasesF) > limite_deuda) {
                                    mnsj = "A excedido su deuda no puede tomar la clase";
                                } else {
                                    BasicDBObject newDocument2 = new BasicDBObject();
                                    newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + pago_clasesF));
                                    BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                    coll2.update(qry2, newDocument2);
                                    bandP = true;
                                }
                            }
                        }
                        //termina area de fin de semana
                    } else {
                        //area
                        if (diaS == 1 || diaS == 6 || diaS == 7) {
                            if (band == true) {
                                if (saldo > 0) {
                                    if ((saldo - pago_clasesF) < 0) {
                                        int aux = (saldo - pago_clasesF) * -1;
                                        if (limite_deuda <= aux) {
                                            //Se actualiza saldo a 0
                                            BasicDBObject newDocument = new BasicDBObject();
                                            newDocument.append("$set", new BasicDBObject().append("saldo", 0));
                                            BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                            coll2.update(qry, newDocument);
                                            //se actualiza deuda agregando la deuda
                                            BasicDBObject newDocument2 = new BasicDBObject();
                                            newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + aux));
                                            BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                            coll2.update(qry2, newDocument2);

                                            bandP = true;
                                        } else {
                                            mnsj = "A excedido su deuda no puede tomar la clase";
                                        }
                                    } else {
                                        //Se resta solamente el pago a saldo
                                        BasicDBObject newDocument = new BasicDBObject();
                                        newDocument.append("$set", new BasicDBObject().append("saldo", saldo - pago_clasesF));
                                        BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry, newDocument);
                                        bandP = true;
                                    }
                                } else {
                                    //se genera deuda y se compara con limite de deuda
                                    if ((deuda + pago_clasesF) > limite_deuda) {
                                        mnsj = "A excedido su deuda no puede tomar la clase";
                                    } else {
                                        BasicDBObject newDocument2 = new BasicDBObject();
                                        newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + pago_clasesF));
                                        BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry2, newDocument2);
                                        bandP = true;
                                    }
                                }
                            }
                        } else {
                            if (band == true) {
                                if (saldo > 0) {
                                    if ((saldo - pago_clasesN) < 0) {
                                        int aux = (saldo - pago_clasesN) * -1;
                                        if (limite_deuda <= aux) {
                                            //Se actualiza saldo a 0
                                            BasicDBObject newDocument = new BasicDBObject();
                                            newDocument.append("$set", new BasicDBObject().append("saldo", 0));
                                            BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                            coll2.update(qry, newDocument);
                                            //se actualiza deuda agregando la deuda
                                            BasicDBObject newDocument2 = new BasicDBObject();
                                            newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + aux));
                                            BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                            coll2.update(qry2, newDocument2);

                                            bandP = true;
                                        } else {
                                            mnsj = "A excedido su deuda no puede tomar la clase";
                                        }
                                    } else {
                                        //Se resta solamente el pago a saldo
                                        BasicDBObject newDocument = new BasicDBObject();
                                        newDocument.append("$set", new BasicDBObject().append("saldo", saldo - pago_clasesN));
                                        BasicDBObject qry = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry, newDocument);
                                        bandP = true;
                                    }
                                } else {
                                    //se genera deuda y se compara con limite de deuda
                                    if ((deuda + pago_clasesN) > limite_deuda) {
                                        mnsj = "A excedido su deuda no puede tomar la clase";
                                    } else {
                                        BasicDBObject newDocument2 = new BasicDBObject();
                                        newDocument2.append("$set", new BasicDBObject().append("deuda", deuda + pago_clasesN));
                                        BasicDBObject qry2 = new BasicDBObject().append("id", Integer.parseInt(txtReistraClase.getText()));
                                        coll2.update(qry2, newDocument2);
                                        bandP = true;
                                    }
                                }
                            }
                        }
                    }
                    if (band == true) {
                        if (bandP == true) {

                            doc.put("id", Integer.parseInt(txtReistraClase.getText()));
                            doc.put("nombre", o.get("nombre"));
                            doc.put("fecha_Asis", dateFormat.format(fecha));
                            doc.put("hora", hourFormat.format(fecha));
                            if (diaS == 1 || diaS == 6 && diaS == 7) {
                                doc.put("finSemana", "si");
                            } else {
                                doc.put("finSemana", "no");
                            }
                            coll.insert(doc);
                            verAlumnos();
                            JOptionPane.showMessageDialog(null, "Registro de asistencia correcto");
                        } else {
                            JOptionPane.showMessageDialog(null, mnsj);
                            verAlumnos();
                        }
                    } else {
                        verAlumnos();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "El numero de alumno no existe en el registro");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error: Ingrese un numero de alumno", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error: No debe ingresar letras", "Alerta", JOptionPane.WARNING_MESSAGE);
        } catch (Error e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Alerta", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistroClases.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistroClases.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistroClases.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistroClases.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistroClases().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnVerById;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtBuscarXDia;
    private javax.swing.JTextField txtIdAlumno;
    private javax.swing.JTextField txtReistraClase;
    // End of variables declaration//GEN-END:variables
}
